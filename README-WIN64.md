# Getting started YOLO v3 on Windows 10

- 0. We assume that you have NVIDIA GPU with CUDA support in your computer because YOLO v3 consumes a lot of memory and CPU/GPU resources. 
- 1. Install miniconda: https://repo.anaconda.com/miniconda/Miniconda3-latest-Windows-x86_64.exe
     or Anaconda from https://www.anaconda.com/products/individual
- 2. Install CUDA 10.1 https://developer.nvidia.com/cuda-10.1-download-archive-base?target_os=Windows&target_arch=x86_64&target_version=10&target_type=exenetwork
(actually CUDA 10.2 or 11.0 are also possible, but I've tested 10.1 only)
- 3. Unzip cuDNN/cudnn-10.1-windows10-x64-v7.6.5.32.zip to C:\cuDNN and **add path C:\cuDNN\bin to PATH environment variable** (https://gitlab.com/vfuga/PyTorch-YOLOv3/-/blob/windows/cuDNN/cudnn-10.1-windows10-x64-v7.6.5.32.zip)
- 4. I use VSCode https://code.visualstudio.com/download
  but you may take any IDE for python, which supports conda virtual envitonments
- 6. Install Anaconda - https://www.anaconda.com/products/individual#windows
- 5. Run create-env.bat (it creates python's virtual environment for pythorch)

# Prepare dataset

- download https://pjreddie.com/media/files/train2014.zip   (13 GB)
- download https://pjreddie.com/media/files/val2014.zip  (6 GB)
- download https://pjreddie.com/media/files/coco/labels.tgz  (17 MB)
- unzip all archives somewhere (I hope it will be an SSD drive)
````
    C:\COCO\
            |-images\
            |        |-train2014\
            |        |           |COCO_train2014_00000009.jpg
            |        |           |...
            |        |-val2014\
            |                  |COCO_val2014_000000042.jpg
            |                  |...
            |-labels\
                     |-train2014\
                     |           |COCO_train2014_00000009.txt
                     |           |...
                     |-val2014\
                               |COCO_val2014_000000042.txt
                               |...
````
- 6. Download weights
     - https://pjreddie.com/media/files/yolov3.weights
     - https://pjreddie.com/media/files/yolov3-tiny.weights
     - https://pjreddie.com/media/files/darknet53.conv.74
- 6.1 Copy them to PyTorch-YOLOv3\weights dir
- 7. start cmd.exe and after activating pytorch virtual environment start jupyter notebook this way:
````
conda activate py37-torch
jupyter notebook --ip=127.0.0.1 --port=9999

````
- 8. You may use jupyter notebooks as examples of code for training/evaluating YOLOv3 model


