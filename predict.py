from utils.datasets import resize
from utils.transforms import DEFAULT_TRANSFORMS  # get default transforms
from utils.utils import load_classes
from utils.parse_config import parse_data_config
from models import Darknet
from test import evaluate
import torch.optim as optim
from torch.autograd import Variable
from torchvision import transforms
from torchvision import datasets
from torch.utils.data import DataLoader
import torch
import pandas as pd
import glob
import os
import sys
import re
from tqdm.auto import tqdm
import datetime as dt
import numpy as np

import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle

START_TS = dt.datetime.now()


MODEL_DEFINITION = "config\\yolov3.cfg"
DARKNET_WEIGHTS = "weights\\yolov3.weights"
CLASS_NAMES = "data\\coco.names"
IoU_THRESHOLD = 0.5
OBJECT_CONFIDENCE_THRESHOLD = 0.5
NMS_THRESHOLD = 0.4
YOLO_IMAGE_SIZE = 416

image_path = "C:/COCO/images/val2014/COCO_val2014_000000000042.jpg"
#image_path = "C:/COCO/images/val2014/COCO_val2014_000000000133.jpg"
#image_path = "C:/COCO/images/val2014/COCO_val2014_000000000192.jpg"
#image_path = "C:/COCO/images/val2014/COCO_val2014_000000005535.jpg"


def transform_predict(model, img:np.ndarray, ann_boxes:np.ndarray=None):
    
    # get default transforms
    from utils.transforms import DEFAULT_TRANSFORMS
    from utils.datasets import resize
    
    if ann_boxes is None: # specify dummy bounding box
        ann_b = np.array([[0      ,  0.5,  0.5,  .99, .99 ]])
    else:
        ann_b = ann_boxes

    model.eval()
    
    TensorType = torch.cuda.FloatTensor if torch.cuda.is_available() else torch.FloatTensor

    t_image, t_targets = DEFAULT_TRANSFORMS((img.copy(), ann_b.copy()))
    t_image = resize(t_image, YOLO_IMAGE_SIZE).unsqueeze(dim=0).to(device)
    t_image = Variable(t_image.type(TensorType), requires_grad=False)
    # print('targets:', t_targets)
    
    with torch.no_grad():
        predictions = model(t_image)
        
    # print(t_image.mean(), t_image.sum(), t_image[0][0].sum(), predictions.sum(), t_image.shape)
    if ann_boxes is None:
        t_targets = None
    
    return {
        'predictions': predictions.numpy().copy(),
        'image': t_image.cpu().numpy().copy(),
        'targets': t_targets
    }

def predict(model, img:np.ndarray, ann_boxes:np.ndarray=None):
    return transform_predict(model, img, ann_boxes)['predictions']

if __name__ == "__main__":
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    class_names = load_classes(CLASS_NAMES)
    model = Darknet(MODEL_DEFINITION).to(device)
    model.load_darknet_weights(DARKNET_WEIGHTS)

    img = plt.imread(image_path)
    annotation_path = image_path.replace(
        '/images/', '/labels/').replace('.jpg', '.txt')
    ann_boxes = np.loadtxt(annotation_path)
    if len(ann_boxes.shape) == 1:
        ann_boxes = np.expand_dims(ann_boxes, 0)

    predictions = predict(model, img, ann_boxes)
    print(predictions.shape)
