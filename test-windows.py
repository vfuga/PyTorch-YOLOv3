import pandas as pd
import glob
import os, sys, re
import tqdm.auto as tqdm 
import datetime as dt

from models import *
from utils.utils import *
from utils.datasets import *
from utils.augmentations import *
from utils.transforms import *
from utils.parse_config import *

import torch
from torch.utils.data import DataLoader
from torchvision import datasets
from torchvision import transforms
from utils.utils import  TQDM_BAR_FORMAT

from multiprocessing import freeze_support

def evaluate(model=None, dataloader=None, iou_thres=0.5, conf_thres=0.5, nms_thres=0.5, img_size=416, batch_size=8):
    """
        evaluate dataset
    """
    if model is None:
        raise RuntimeError('model is not specified')
    if dataloader is None:
        raise RuntimeError('dataloader is not specified')

    model.eval()

    Tensor = torch.cuda.FloatTensor if torch.cuda.is_available() else torch.FloatTensor

    labels = []
    sample_metrics = []  # List of tuples (TP, confs, pred)

    for batch_i, (fnames, imgs, targets) in enumerate(tqdm.tqdm(dataloader, desc="Evaluate:", bar_format=TQDM_BAR_FORMAT)):
        
        if targets is None:
            continue
            
        # Extract labels
        labels += targets[:, 1].tolist()
        # Rescale target
        targets[:, 2:] = xywh2xyxy(targets[:, 2:])
        targets[:, 2:] *= img_size

        imgs = Variable(imgs.type(Tensor), requires_grad=False)

        with torch.no_grad():
            outputs = model(imgs)
            outputs = non_max_suppression(outputs, conf_thres=conf_thres, nms_thres=nms_thres)

        sample_metrics += get_batch_statistics(outputs, targets, iou_threshold=iou_thres)
   
    if len(sample_metrics) == 0:  # no detections over whole validation set.
        print('No detections over whole validation set.')
        return None
    
    # Concatenate sample statistics
    true_positives, pred_scores, pred_labels = [np.concatenate(x, 0) for x in list(zip(*sample_metrics))]
    precision, recall, AP, f1, ap_class = ap_per_class(true_positives, pred_scores, pred_labels, labels)
    results = {
        'precision': precision,
        'recall': recall,
        'AP': AP,
        'f1': f1,
        'ap_class': ap_class
    }

    return results


if __name__ == '__main__':
    freeze_support()

    START_TS = dt.datetime.now()
    COCO_DATASET_DIR = 'C:\\COCO\\images\\val2014\\'
    YOLO_MODEL_DEFINITION = 'config\\yolov3.cfg'
    IMAGES_PATHS = 'data\\coco_validation.txt'
    NUM_WORKERS = 0
    BATCH_SIZE = 1
    IMAGE_SIZE = 416 

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    # Get dataloader
    eval_dataset = ListDataset(IMAGES_PATHS, img_size=IMAGE_SIZE, multiscale=False, transform=DEFAULT_TRANSFORMS)

    eval_dataloader = torch.utils.data.DataLoader(
        eval_dataset, 
        batch_size=BATCH_SIZE,
        shuffle=False,
        num_workers=NUM_WORKERS,
        collate_fn=eval_dataset.collate_fn
    )

    # Load model 
    yolov3_model = Darknet(YOLO_MODEL_DEFINITION).to(device)
    yolov3_model.load_darknet_weights('weights/yolov3.weights')
    # print(model)
    
    res = evaluate(model=yolov3_model, dataloader=eval_dataloader)
    print('mAP:', res['AP'].mean())

