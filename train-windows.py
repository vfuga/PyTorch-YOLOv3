from __future__ import division

from models import *
from utils.logger import *
from utils.utils import *
from utils.datasets import *
from utils.augmentations import *
from utils.transforms import *
from utils.parse_config import *
from utils.utils import TQDM_BAR_FORMAT

from test import evaluate
import datetime as dt
import glob
import gc, re


from terminaltables import AsciiTable

import os
import sys
import time
import datetime
import argparse
from tqdm.auto import tqdm

import torch
from torch.utils.data import DataLoader
from torchvision import datasets
from torchvision import transforms
from torch.autograd import Variable
import torch.optim as optim

from multiprocessing import freeze_support

if __name__ == '__main__':
    freeze_support()

    parser = argparse.ArgumentParser()
    parser.add_argument("--epochs", type=int, default=2, help="number of epochs")
    parser.add_argument("--batch_size", type=int, default=3, help="size of each image batch")
    parser.add_argument("--gradient_accumulations", type=int, default=40, help="number of gradient accums before step")
    parser.add_argument("--model_def", type=str, default="config/yolov3.cfg", help="path to model definition file")
    parser.add_argument("--data_config", type=str, default="config/coco.data", help="path to data config file")
    parser.add_argument("--pretrained_weights", type=str, help="if specified starts from checkpoint model")
    parser.add_argument("--n_cpu", type=int, default=3, help="number of cpu threads to use during batch generation")
    parser.add_argument("--img_size", type=int, default=416, help="size of each image dimension")
    parser.add_argument("--checkpoint_interval", type=int, default=1, help="interval between saving model weights")
    parser.add_argument("--evaluation_interval", type=int, default=1, help="interval evaluations on validation set")
    parser.add_argument("--compute_map", default=True, help="if True computes mAP every tenth batch")
    parser.add_argument("--multiscale_training", default=True, help="allow for multi-scale training")
    parser.add_argument("--verbose", "-v", default=True, action='store_true', help="Makes the training more verbose")
    parser.add_argument("--logdir", type=str, default="logs", help="Defines the directory where the training log files are stored")
    
    opt = parser.parse_args("")
    
    print(opt)
    
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    print('device:', device)
    
    logger = Logger(opt.logdir)
    
    os.makedirs("output", exist_ok=True)
    os.makedirs("checkpoints", exist_ok=True)
    
    data_config = {'gpus': '0',
                   'num_workers': '1',
                   'classes': '80',
                   'train': 'data/coco_train.txt',
                   'valid': 'data/coco_validation.txt',
                   'names': 'data/coco.names',
                   'backup': 'backup/',
                   'eval': 'coco'}
    
    train_path = data_config["train"]
    valid_path = data_config["valid"]
    class_names = load_classes(data_config["names"])

    print(data_config)
    
    model = Darknet(opt.model_def).to(device)
    _ = model.apply(weights_init_normal)

    pytorch_total_params = sum(p.numel() for p in model.parameters())
    print('---- Total number of parameters:', pytorch_total_params)
    
    last_epoch = 0
    if opt.pretrained_weights:
        if opt.pretrained_weights.endswith(".pth"):
            print('Load pretrained weights from pth')
            model.load_state_dict(torch.load(opt.pretrained_weights))
        else:
            print('Load pretrained weights')
            model.load_darknet_weights(opt.pretrained_weights)
    else:
        chkpt_files = sorted(glob.glob('checkpoints\\*.pth'))
        if len(chkpt_files) > 0:
            last_epoch = int(chkpt_files[-1].strip('checkpoints\\yolov3_ckpt_').strip('.pth'))
            print('Loading checkpoit file:', chkpt_files[-1], f", last_epoch: {last_epoch}")
            model.load_state_dict(torch.load(chkpt_files[-1]))
            last_epoch += 1
        else:
            print('Skip loading weights')
        
    # Get dataloader
    train_dataset = ListDataset(train_path, multiscale=opt.multiscale_training, transform=AUGMENTATION_TRANSFORMS)
    train_dataloader = torch.utils.data.DataLoader(
        train_dataset,
        batch_size=opt.batch_size,
        shuffle=True,
        num_workers=2,
        pin_memory=True,
        collate_fn=train_dataset.collate_fn,
    )
    
    optimizer = torch.optim.Adam(model.parameters())

    metrics = [
            "grid_size",
            "loss",
            "x",
            "y",
            "w",
            "h",
            "conf",
            "cls",
            "cls_acc",
            "recall50",
            "recall75",
            "precision",
            "conf_obj",
            "conf_noobj",
    ]
    
    _ = model.train()
    
    start_time = dt.datetime.now()
    print(start_time)
    
    print('(Dataset, train_dataloader)', (len(train_dataloader), len(train_dataset)))
    print('Number of epocs:', opt.epochs)


    for epoch in range(last_epoch, last_epoch + opt.epochs):

        train_loss = 0.0
        mAP = 0.0
        start_ts = datetime.datetime.now()

        for batch_i, (fnames, imgs, targets) in enumerate(tqdm(train_dataloader, desc=f"Training Epoch {epoch}", bar_format=TQDM_BAR_FORMAT)):
            batches_done = len(train_dataloader) * epoch + batch_i
            
            imgs = Variable(imgs.to(device))
            targets = Variable(targets.to(device), requires_grad=False)
    
            loss, outputs = model(imgs, targets)
            train_loss += loss.item()
    
            # Backpropagation
            loss.backward()
    
            if batches_done % opt.gradient_accumulations == 0:
                # Accumulates gradient before each step
                optimizer.step()
                optimizer.zero_grad()
    
        gc.collect()
        
        if epoch % opt.evaluation_interval == 0:
            print("\n---- Evaluating Model ----")
            # Evaluate the model on the validation set
            results = evaluate(
                model,
                path=valid_path,
                iou_thres=0.5,
                conf_thres=0.5,
                nms_thres=0.5,
                img_size=opt.img_size,
                batch_size=4
            )

            if results is None:
                print("Cannot evaluate")
                continue
            else:
                precision, recall, AP, f1, ap_class = results
    
                evaluation_metrics = [
                    ("validation/precision", precision.mean()),
                    ("validation/recall", recall.mean()),
                    ("validation/mAP", AP.mean()),
                    ("validation/f1", f1.mean()),
                ]
                logger.list_of_scalars_summary(evaluation_metrics, epoch)
                # Print class APs and mAP
                ap_table = [["Index", "Class name", "AP"]]
                for i, c in enumerate(ap_class):
                    ap_table += [[c, class_names[c], "%.5f" % AP[i]]]
                print(AsciiTable(ap_table).table)
                mAP = AP.mean()
                print(f"---- mAP: {mAP}, mean(loss): {train_loss/len(train_dataset)} ----")
    
        if epoch % opt.checkpoint_interval == 0:
            checkpoint_file = f"checkpoints/yolov3_ckpt_%04d.pth" % epoch
            print('Writing checkpoit file:', checkpoint_file)
            
            with open("train.log", "a") as f:
                f.write(f"{checkpoint_file},{mAP},{train_loss/len(train_dataset)},{datetime.datetime.now()-start_ts}\n")
                f.flush()
            
            torch.save(model.state_dict(), checkpoint_file)

        gc.collect()
