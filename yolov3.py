import glob
import os
import sys
import re
from tqdm.auto import tqdm
import datetime as dt
import numpy as np
import pandas as pd

import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle

import torch
from torch.utils.data import DataLoader
from torchvision import datasets
from torchvision import transforms
from torch.autograd import Variable
import torch.optim as optim

from models import Darknet
from utils.parse_config import parse_data_config
from utils.utils import load_classes, rescale_boxes

MODEL_DEFINITION = "config\\yolov3.cfg"
DARKNET_WEIGHTS = "weights\\yolov3.weights"
CLASS_NAMES = "data\\coco.names"
IOU_THRESHOLD = 0.5
OBJECT_CONFIDENCE_THRESHOLD = 0.5
NMS_THRESHOLD = 0.4
YOLO_IMAGE_SIZE = 416

class YOLOv3():
    """
        YOLOv3 demonstrator 
    """
    WIDTH_SIZE_INCHES = 10
    FONT_SIZE = 12

    def __init__(self,
        model_cfg_path = MODEL_DEFINITION,
        weights_file_path = DARKNET_WEIGHTS,
        class_names_file = CLASS_NAMES,
        confidence_threshold = .5,
        iou_threshold = IOU_THRESHOLD,
        nms_threshold = NMS_THRESHOLD,
        yolo_image_size = YOLO_IMAGE_SIZE
    ):
        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        self.class_names = load_classes(class_names_file)
        self.model = Darknet(model_cfg_path).to(self.device)
        self.model.load_darknet_weights(weights_file_path)
        self.nms_threshold = nms_threshold
        self.iou_threshold = iou_threshold
        self.confidence_threshold = confidence_threshold

    def bbox_iou(self, box1, box2, x1y1x2y2=True):
        """
        Returns the IoU of two bounding boxes: numpy version
        
        """
        if not x1y1x2y2:
            # Transform from center and width to exact coordinates
            b1_x1, b1_x2 = box1[:, 0] - box1[:, 2] / 2, box1[:, 0] + box1[:, 2] / 2
            b1_y1, b1_y2 = box1[:, 1] - box1[:, 3] / 2, box1[:, 1] + box1[:, 3] / 2
            b2_x1, b2_x2 = box2[:, 0] - box2[:, 2] / 2, box2[:, 0] + box2[:, 2] / 2
            b2_y1, b2_y2 = box2[:, 1] - box2[:, 3] / 2, box2[:, 1] + box2[:, 3] / 2
        else:
            # Get the coordinates of bounding boxes
            b1_x1, b1_y1, b1_x2, b1_y2 = box1[:, 0], box1[:, 1], box1[:, 2], box1[:, 3]
            b2_x1, b2_y1, b2_x2, b2_y2 = box2[:, 0], box2[:, 1], box2[:, 2], box2[:, 3]
            
        # get the corrdinates of the intersection rectangle
        inter_rect_x1 = np.maximum(b1_x1, b2_x1)
        inter_rect_y1 = np.maximum(b1_y1, b2_y1)
        inter_rect_x2 = np.minimum(b1_x2, b2_x2)
        inter_rect_y2 = np.minimum(b1_y2, b2_y2)
        
        # Intersection area
        inter_area = np.maximum(inter_rect_x2 - inter_rect_x1 + 1, 0) * np.maximum(inter_rect_y2 - inter_rect_y1 + 1, 0)
    
        # Union Area
        b1_area = (b1_x2 - b1_x1 + 1) * (b1_y2 - b1_y1 + 1)
        b2_area = (b2_x2 - b2_x1 + 1) * (b2_y2 - b2_y1 + 1)
    
        iou = inter_area / (b1_area + b2_area - inter_area + 1e-16)

        return iou

    def xywh2xyxy(self, t):
        r = t.copy()
        r[..., 0] = t[..., 0] - t[..., 2] / 2
        r[..., 1] = t[..., 1] - t[..., 3] / 2
        r[..., 2] = t[..., 0] + t[..., 2] / 2
        r[..., 3] = t[..., 1] + t[..., 3] / 2
        return r


    def non_max_suppression(self, prediction):
        prediction[...,:4] = self.xywh2xyxy(prediction[...,:4])  

        output = [None]   

        image_pred =prediction[0]

        # Filter out confidence scores below threshold
        image_pred = image_pred[image_pred[:, 4] >= self.confidence_threshold]   # imape_pred[4] is object_confidence_field

        # Object confidence times class confidence    
        score = image_pred[:, 4] * image_pred[:, 5:].max(1)[0]  #Умножаем object_confidence_field на class_confidence для всех классов и узнаем максимальный скор для класса

        # Sort by it в убывающем порядке
        image_pred = image_pred[(-score).argsort()]

        class_confs = image_pred[:, 5:].max(1, keepdims=True)
        class_preds = np.expand_dims(image_pred[:, 5:].argmax(1),1)

        detections= np.concatenate([image_pred[:,:5], class_confs, class_preds], axis=1)

        # Perform non-maximum suppression
        keep_boxes = []
        while len(detections):
            large_overlap = self.bbox_iou(np.expand_dims(detections[0, :4], 0), detections[:, :4]) > self.nms_threshold
            label_match = detections[0, -1] == detections[:, -1]
            # Indices of boxes with lower confidence scores, large IOUs and matching labels
            invalid = large_overlap & label_match
            weights = detections[invalid, 4:5]
            # Merge overlapping bboxes by order of confidence
            detections[0, :4] = (weights * detections[invalid, :4]).sum(0) / weights.sum()
            keep_boxes += [detections[0]]
            detections = detections[~invalid]

        if keep_boxes:
            output[0] = np.vstack(keep_boxes)
            
        return output[0]


    def transform_predict(self, img:np.ndarray, ann_boxes:np.ndarray=None):
        
        # get default transforms
        from utils.transforms import DEFAULT_TRANSFORMS
        from utils.datasets import resize
        
        if ann_boxes is None: # specify dummy bounding box
            ann_b = np.array([[0      ,  0.5,  0.5,  .99, .99 ]])
        else:
            ann_b = ann_boxes

        self.model.eval()
        
        TensorType = torch.cuda.FloatTensor if torch.cuda.is_available() else torch.FloatTensor

        t_image, t_targets = DEFAULT_TRANSFORMS((img.copy(), ann_b.copy()))
        t_image = resize(t_image, YOLO_IMAGE_SIZE).unsqueeze(dim=0).to(self.device)
        t_image = Variable(t_image.type(TensorType), requires_grad=False)
        # print('targets:', t_targets)
        
        with torch.no_grad():
            predictions = self.model(t_image)
            
        # print(t_image.mean(), t_image.sum(), t_image[0][0].sum(), predictions.sum(), t_image.shape)
        if ann_boxes is None:
            t_targets = None
        
        return {
            'predictions': predictions.numpy().copy(),
            'image': t_image.cpu().numpy().copy(),
            'targets': t_targets
        }

    def predict(self, img:np.ndarray, ann_boxes:np.ndarray=None):
        return self.transform_predict(img, ann_boxes)['predictions']

    def draw_bboxes(self, img:np.ndarray, ann_boxes:np.ndarray=None, pred_boxes:np.ndarray=None):

        w = img.shape[1]
        h = img.shape[0]
        plt.figure(figsize=(YOLOv3.WIDTH_SIZE_INCHES,img.shape[0]/img.shape[1]*YOLOv3.WIDTH_SIZE_INCHES))
        plt.imshow(img)
        axes = plt.gca()
            
        if ann_boxes is not None:
            
            if len(ann_boxes.shape) == 1:
                ann_boxes = np.expand_dims(ann_boxes,0)

            for b in ann_boxes:
                _c, _x, _y, _w, _h = b
                                    
                box = Rectangle((int((_x-_w/2)*w), int((_y-_h/2)*h)), 
                                int(_w*w), 
                                int(_h*h), 
                                linewidth=2, edgecolor='red', 
                                facecolor=(1,0,0,0.2),
                            )
                axes.text(int((_x-_w/2)*w) + 4, 
                          int((_y-_h/2)*h) + YOLOv3.FONT_SIZE + 2, 
                          self.class_names[int(_c)], 
                          fontsize=YOLOv3.FONT_SIZE, 
                          bbox=dict(color='red', alpha=0.5), 
                          color='white')
                
                axes.add_patch(box)
            
        if pred_boxes is not None:
            for b in pred_boxes:
                _c, _x, _y, _w, _h = b
                box = Rectangle((int((_x-_w/2)*w), int((_y-_h/2)*h)), 
                                int(_w*w), 
                                int(_h*h), 
                                linewidth=2, edgecolor='blue', 
                                facecolor=(0,1,0,0.2))
                
                axes.text(int((_x-_w/2)*w) + 4, 
                          int((_y-_h/2)*h) + YOLOv3.FONT_SIZE + 2, 
                          self.class_names[int(_c)], 
                          fontsize=YOLOv3.FONT_SIZE, 
                          bbox=dict(color='blue', alpha=0.5), 
                          color='white')
                
                axes.add_patch(box)
        
        plt.show()


    def __call__( self, 
        image_path = "C:/COCO/images/val2014/COCO_val2014_000000000192.jpg",
        annotations_path = None, #
        verbose=False
    ):
        img = plt.imread(image_path)
        try:
            ann_boxes = np.loadtxt(annotations_path)
            if len(ann_boxes.shape) == 1:
                ann_boxes = np.expand_dims(ann_boxes,0)
        except:
            ann_boxes = None

        outputs = self.transform_predict(img, ann_boxes)
        transformed_image = outputs['image']
        try:
            targets = outputs['targets'][:, 1:]
            # display(outputs)
        except:
            targets = None
            
        predicted_bb = self.non_max_suppression(outputs['predictions'])

        detections = rescale_boxes(predicted_bb, YOLO_IMAGE_SIZE, img.shape[:2])
        display_boxes = np.zeros((detections.shape[0], 5))
        display_boxes[:,0] = detections[:, 6]
        display_boxes[:,1] = (detections[:, 0] + detections[:, 2])/2.0/img.shape[1]
        display_boxes[:,2] = (detections[:, 1] + detections[:, 3])/2.0/img.shape[0]
        display_boxes[:,3] = (detections[:, 2] - detections[:, 0])/img.shape[1]
        display_boxes[:,4] = (detections[:, 3] - detections[:, 1])/img.shape[0]

        if verbose:
            self.draw_bboxes(img=img, ann_boxes=ann_boxes, pred_boxes=display_boxes)
        return display_boxes


if __name__ == "__main__":
    yolov3 = YOLOv3()
    yolov3(verbose=True)

